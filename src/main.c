/* Завдяння Під номером 26. Дано двовимірний масив з 𝑁 × 𝑁 цілих чисел. Помножити матрицю саму на себе
(відповідно до правил множення матриць).
 */

#include <stdio.h>
//Створюємо константу, що буде визначати кількість рядків та стовпчиков у матриці
#define SIZE_OF_ROWS_AND_COLUMNS 3

//Точка входа у программу
int main() {

  //Створюємо двовимірниц массив (тобто матрицю) згідно заданих розмірів. Матриця квадратна. Заповнюємо матрицю данними
  int matrix[SIZE_OF_ROWS_AND_COLUMNS][SIZE_OF_ROWS_AND_COLUMNS] = {
    {2, 3, 4},
    {5, 4, 1},
    {0, 2, 3}
  };

  //Створюємо матрицю, яка буде вміщати в себе результат множення початкової матриці
  int result_matrix[SIZE_OF_ROWS_AND_COLUMNS][SIZE_OF_ROWS_AND_COLUMNS] = {
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0}
  };

  /*За допомогою данних циклів перебираємо елементи матриць,
  цикли допомагають нам зробити множення матриці згідно правил
  */
  for (int i = 0; i < SIZE_OF_ROWS_AND_COLUMNS; i++) {
    for (int j = 0; j < SIZE_OF_ROWS_AND_COLUMNS; j++) {
        result_matrix[i][j] = 0;
        for (int k = 0; k < SIZE_OF_ROWS_AND_COLUMNS; k++) {
            result_matrix[i][j] += matrix[i][k] * matrix[k][j];
        }
    }
  }

  return 0;
}

