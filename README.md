# Лабораторна робота №6. Масиви

## Мета
Виконати одне завдання з пулу завдань на свій розсуд згідно її складності

## 1 Вимоги

### 1.1 Розробник
Інформація про розробника:
- Авдєєва Софія Олександрівна 
- Група КІТ 121Б

### 1.2 Загальне завдання
- Дано двовимірний масив з 𝑁 × 𝑁 цілих чисел. Помножити матрицю саму на себе (відповідно до правил множення матриць).

### 1.3 Задача
- Створити програму, що буде множити матрицю саму на себе відповідно до правил множення матриць.

## Як користуватися
- Gitlab https://gitlab.com/sonaavdeeva68/lab06

> - Файл з вихідним кодом знаходиться у папкі dist, якщо її немає - вам потрібно скомпілювати проект
> - Точка входу у проект знаходиться в файлі src/main.c. Вона повинна бути одна, бо компілятор повинен знати, де починати роботу з компіляції
> - Скомпілювати проект можна за допомогою команди make run
> - Для того, щоб переглянути результат множення поданої матриці саму на себе, вам потрібно за допомогою дебаггера зробити breakpoints на 38 строчці
> - Коли программа зупиняеться на breakpoint - потрібно вивести всі змінні за допомогою команди v.
> - Результат:
``` c
(int [3][3]) matrix = {
  [0] = ([0] = 2, [1] = 3, [2] = 4)
  [1] = ([0] = 5, [1] = 4, [2] = 1)
  [2] = ([0] = 0, [1] = 2, [2] = 3)
}
(int [3][3]) result_matrix = {
  [0] = ([0] = 19, [1] = 26, [2] = 23)
  [1] = ([0] = 30, [1] = 33, [2] = 27)
  [2] = ([0] = 10, [1] = 14, [2] = 11)
}
```

## Висновок
На цій лабораторній роботі я розобила програму, яка мноєить матрийю сама на себе відповідно до правил множення матриць.
